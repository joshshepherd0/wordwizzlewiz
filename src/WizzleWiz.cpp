#include "../include/WizzleWiz.hpp"

/*
index is a member of [1,3]
*/
int WizzleWiz::runTest(int index)
{
	vector<vector<char>> matrix;
	vector<int> targetWordLengths;
	switch (index)
	{
	case 1:
		matrix = {	{ 'w','t','a','r' },
					{ 's','e','a','s' },
					{ 'l',' ','e','t' },
					{ 'b','o','o','d' } };
		targetWordLengths = { 5,5,5 };
		break;

	case 2:
		matrix = {	{ 's','o','a','i','f' },
					{ 'f','o','d','a','l' },
					{ 'l','t','w','r','e' },
					{ 'y','n','e','e','s' },
					{ 'b','t','w','o','d' } };
		targetWordLengths = { 4,8,5,8 };
		break;

	case 3:
		matrix = {	{ 'b','i','m','o' },
					{ 'r','e','a','l' },
					{ 'r','k','f','h' },
					{ 'o','o','o','j' } };
		targetWordLengths = { 4,4,4,4 };
		break;
	case 4:
	//http://wordwhizzle-answers.com/wp-content/uploads/2016/10/Wordwhizzle-Hippo-443-1.jpg
	// even with 10 minutes of runtime, this has never been solved
	// TODO multithread. Luckily this is all highly parallelizable
		matrix = {	{ 'k','e','r',' ',' ' },
					{ 'e','a','u','n','a' },
					{ 'p','t','e','r','e' },
					{ 'o','s','k','t','r' },
					{ 'd','p','i','c','a' } };
		targetWordLengths = { 4,5,4,7,3 };
		break;
	case 5:
		//http://www.gamesolver.net/wp-content/uploads/2016/09/Wordwhizzle-Whale-Level-423.jpg
		matrix = {	{ 'y','w','e','m',' ' },
					{ 'a','g','i','h',' ' },
					{ 't','t','n','o','p' },
					{ 'w','s','o','e','f' },
					{ 'a','i','n','r',' ' } };
		targetWordLengths = { 5,3,9,5 };
		break;
	default: 
		cout << "test " << index << " does not exist" << endl;
		return -1;
	}
	Board board(matrix);
	board.print();
	Solver solver(board, targetWordLengths);
	solver.loadDictionary(fname);
	solver.generateSolutions();
	solver.printUniqSolutions();
	return 0;
}
