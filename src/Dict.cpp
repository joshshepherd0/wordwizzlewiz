#include "../include/Dict.hpp"

void Dict::clear()
{
	dict_.clear();
}

// adds the given string to the dictionary
void Dict::addWord(string word)
{
	dict_[word] = true;
}

int Dict::size()
{
	return dict_.size();
}


/*

determines whether a given string representing the beginning of a word, is in the dictionary (case-sensitive)

inputs: - a prefix of a potential english word

returns true if there exists such a word such that it starts with the parameter 'prefix'
note:	this differs from containsWord() in that this looks for prefixes (although not exclusively, we could and 
		do match entire words as well), not entire words.

*/
bool Dict::containsPrefix(string prefix)
{
	// find a string in a map by a prefix http://stackoverflow.com/a/9350066/6580675
	map<string, bool>::const_iterator it = dict_.lower_bound(prefix);
	if (it != dict_.end())
	{
		const string& key = it->first;
		if (key.compare(0, prefix.size(), prefix) == 0)
		{
			return true;
		}
	}
	return false;
}

/*

Determines whether a given English word is in the dictionary (case-sensitive)

inputs: - a string

returns true if the parameter string exists in the dictionary as a word
note: this differs from containsPrefix() in that this looks for *entire* words in the dictionary, not prefixes

*/
bool Dict::containsWord(string word)
{
	// TODO can be just binary_search() instead?
	map<string, bool>::const_iterator it = dict_.lower_bound(word);
	if (it != dict_.end())
	{
		const string& key = it->first;
		if (key.compare(word) == 0)
		{
			return true;
		}
	}
	return false;
}

/*
Loads words from a file into the dictionary map. The larger the dictionary, the less likely we are to
mess up a puzzle but also the longer it takes to solve one. Also, it will probably result in more unique solutions
to boards

inputs: - a string corresponding to a file that has a list of words delimited by newline characters

returns -1 on a failure to open the supposed file referred to by fname

*/
int Dict::loadDictionary(string fname)
{
	ifstream infile(fname);
	if (infile.fail())
	{
		return -1;
	}
	string buf;
	while (infile >> buf)
	{
		// there has to be a better way to convert these words to lowercase
		transform(buf.begin(), buf.end(), buf.begin(), ::tolower);
		dict_[buf] = true;
	}
	cout << dict_.size() << " words loaded into dictionary." << endl;
	return 0;
}
