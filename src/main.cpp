/*
 * Joshua Shepherd
 * 2017-02-17
 * 
 * Wizzle Wiz 
 * Program description: This program takes the board and word spaces developed
 * by the popular phone game app, Word Wizzle, and generates all of the possible
 * combinations of words that can solve the puzzle. Some puzzles have several 
 * possible solutions, but most have only one unique possible solution.
 *
 */


#include "../include/WizzleWiz.hpp"

int main(int argc, char ** argv)
{
	cout << "Welcome to Wizzle Wiz!" << endl << "by Joshua Shepherd" << endl;

	// very simple argument parsing
	if (argc > 2 )
	{
		cout << "usage: wizzlewiz <test index>" << endl;
		exit(-1);
	}
	int testIndex = 1;
	if (argc == 2)
	{
		//s_sscanf(argv[1], "%d", &testIndex);
		testIndex = atoi(argv[1]);
	}
	cout << "run test " << testIndex << endl;
	WizzleWiz app;
	app.runTest(testIndex);
	return 0;
}
