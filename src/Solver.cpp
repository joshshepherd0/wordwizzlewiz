#include "../include/Solver.hpp"

Solver::Solver(Board board, vector<int> targetWordLengths)
{
	board_ = board;
	setTargetWordLengths(targetWordLengths);
}

/*

As a summary, prints all of the unique solutions uncovered while solving the puzzle.

*/
void Solver::printUniqSolutions()
{
	if (uniqSolutions_.size() == 0)
	{
		cout << "No solutions were found." << endl;
		cout << "Are you sure you entered the data correctly? A single error can break everything." << endl;
		cout << "Maybe consider expanding the dictionary?" << endl;
		return;
	}
	cout << endl;
	cout << "results" << endl;
	cout << "unique solutions found: " << uniqSolutions_.size() << endl;
	cout << "total solutions found: " << vecSum(numSolutions_) << endl;
	cout << "solutions" << endl;
	int i = 0;
	for (auto uniqSolution : uniqSolutions_)
	{
		cout << i+1 << ":";
		for (string word : uniqSolution)
		{
			cout << " " << word; //prints the words
		}
		//prints the number of different ways said words solved the puzzle board
		cout << " (" << numSolutions_[i] << " permutations)" << endl;
		++i;
	}

}

/*

*/
void Solver::updateMaxAvailableLength()
{
	if (targetWordLengths_.size() == 0)
	{
		// max_element does NOT like being handed an empty vector
		maxAvailableLength_ = 0;
	}
	else
	{
		// set the maximum value in targetWordLengths to maxAvailableLength_
		maxAvailableLength_ = *max_element(begin(targetWordLengths_), end(targetWordLengths_));
	}
}

/*

Inputs: - solution - a vector consisting of words that correctly satisfy the
					puzzle that is the entire board

Returns the index of the already collected unique solutions (uniqSolutions_) 
that this particular solution corresponds to. In the unlikely case that this 
solution is not a duplicate (that is to say just a rearrangement of the order 
of the solution), return -1;

*/
int Solver::getSolutionIndex(vector<string> & solution)
{
	int i = 0;
	for (vector<string> & uniqSolution : uniqSolutions_)
	{
		if (equal(solution.begin(), solution.end(), uniqSolution.begin()))
		{
			// the solution we have found has already been discovered (convergent solutions)
			return i;
		}
		++i;
	}

	// we have not yet discovered the parameter solution (it's not in uniqSolutions (yet))
	return -1;
}

void Solver::setBoard(Board board)
{
	board_ = board;
}

void Solver::setTargetWordLengths(vector<int> & targWordLengths)
{
	targetWordLengths_ = targWordLengths;
	updateMaxAvailableLength();
}


int Solver::loadDictionary(string fname)
{
	dict_.clear();
	return dict_.loadDictionary(fname);
}

void Solver::generateSolutions()
{
	continueWordFromAllPos();
}


/*

After we find a word that potentially solves a portion of the board, we must
recurse with blank words until there aren't any letters left on the board. 
*/
void Solver::findNextWord()
{
	currentSolution_.push_back(prefix_);

	//remove by value http://stackoverflow.com/a/3385249/6580675
	vector<int>::iterator pos = find(targetWordLengths_.begin(), 
																targetWordLengths_.end(), prefix_.length());
	if (pos != targetWordLengths_.end())
	{
		targetWordLengths_.erase(pos);
		updateMaxAvailableLength();
	}
	else
	{
		//TODO throw exception?
		//error
		return ;
	}
	
	if (targetWordLengths_.size() == 0)
	{
		// we have found a solution!
		// that is, we found 1 (or more) words that elimate all of the letters on the board
		saveSolution();
	}
	else
	{
		//save and clear the word for the next cycle
		string oldBegWord = prefix_;
		prefix_.clear();

		continueWordFromAllPos(); 
		
		//restore the word and continue iterating
		prefix_ = oldBegWord;

	}
	targetWordLengths_.push_back(prefix_.length());
	updateMaxAvailableLength();

	vector<string>::iterator pos2 = find(currentSolution_.begin(), 
																				currentSolution_.end(), prefix_);
	if (pos2 != currentSolution_.end()) // == myVector.end() means the element was not found
	{
		currentSolution_.erase(pos2);
	}
}

/*

Congrats! We have successfully found a solution that solves the board. Whether
it's unique (not discovered yet) or the correct what follows.

*/
void Solver::saveSolution()
{
	cout << endl;
	cout << "SUCCESS" << endl;
	cout << "we found a solution:";

	sort(currentSolution_.begin(), currentSolution_.end());

	for (string word : currentSolution_)
	{
		cout << " " << word;
	}
	cout << endl;

	//TODO must we use -1?
	int solutionIndex = getSolutionIndex(currentSolution_);
	if (solutionIndex == -1)
	{
		cout << "the solution is UNIQUE (#1)" << endl;
		uniqSolutions_.push_back(currentSolution_);
		numSolutions_.push_back(1);
	}
	else
	{
		++numSolutions_[solutionIndex];
		cout << "the solution is NOT unique (#" << numSolutions_[solutionIndex] << ")" << endl;
	}
	cout << "num unique solutions: " << uniqSolutions_.size() << endl;
}

/*

inputs: - a solution (vector<string> ) to the board data member

returns true if the this particular solution has not already been discovered

*/
bool Solver::solutionIsUniq(vector<string> & solution)
{
	for (vector<string> & uniqSolution : uniqSolutions_)
	{
		if (equal(solution.begin(), solution.end(), uniqSolution.begin()))
		{
			// the solution we have found has already been discovered (convergent solutions)

			return false;
		}
	}
	return true;
}

/*

for each for the 8 squares surrounding the square (x,y), try to continue adding
letters to the prefix there

*/
void Solver::continueWordInAllDirs(int x, int y)
{
	// check in each direction (read 8 directions) for longer words

	// first check the right column
	//x+1
	if (x + 1 < board_.width)
	{
		if (y + 1 < board_.height)
		{
			continueWordFrom(x + 1, y + 1);
		}
		//y
		continueWordFrom(x + 1, y);
		if (y - 1 >= 0)
		{
			continueWordFrom(x + 1, y - 1);
		}
	}

	// check the center column
	//x
	if (y + 1 < board_.height)
	{
		continueWordFrom(x, y + 1);
	}
	if (y - 1 >= 0)
	{
		continueWordFrom(x, y - 1);
	}

	// check the left column
	//x-1
	if (x - 1 >= 0)
	{
		if (y + 1 < board_.height)
		{
			continueWordFrom(x - 1, y + 1);
		}
		//y
		continueWordFrom(x - 1, y);
		if (y - 1 >= 0)
		{
			continueWordFrom(x - 1, y - 1);

		}
	}
}

/*

Use the letter at position (x,y) to make out prefix longer to the point of
potentially being a full word in the puzzle.
*/
void Solver::continueWordFrom(int x, int y)
{
	if (!board_.isOnBoard(x, y))
	{
		return ;
	}
	char newLetter = board_.getLetter(x, y);
	if (newLetter == ' ')
	{
		// the char at (x,y) will not contribute to finding a new word (set)
		return ;
	}
	int newLen = prefix_.length() + 1;
	if (newLen > maxAvailableLength_)
	{
		// the candidate string is longer than any remaining words
		return ;
	}
	if (!dict_.containsPrefix(prefix_ + newLetter))
	{
		// there isn't a word (in our dictionary at least) that starts with the
		//	current begWord
		return ;
	}

	prefix_ += newLetter;
	board_.clear(x, y);

	if (isAvailableLength(newLen))
	{
		// TODO how can we avoid searching the dictionary twice? (i.e. 
		//	containsPrefix() then containsWord())
		if (dict_.containsWord(prefix_))
		{
			// we found a word!
			//DEBUG SHOW WORDS
			//cout << "found word of sufficient length: " << prefix_ << endl;
			//board_.print();
			findNextWord();
		}
	}

	continueWordInAllDirs(x, y);

	board_.restore(x, y);
	prefix_.resize(prefix_.length() - 1);
}

/*

iterate over every position on the board in order to exhaust every combination
of word strings in the puzzle

*/
void Solver::continueWordFromAllPos()
{
	for (int y = 0; y < board_.height; y++)
	{
		for (int x = 0; x < board_.width; x++)
		{
			continueWordFrom(x, y);
		}
	}
}


/*

intputs - len - the length that we are checking against any remaining lengths

returns true if the input length is potentially one of the remaining lengths on 
the board

*/

bool Solver::isAvailableLength(int len)
{
	std::vector<int>::iterator pos = std::find(targetWordLengths_.begin(), 
																			targetWordLengths_.end(), len);

	return pos != targetWordLengths_.end();
}


/**

Generate solutions to the word wizzle puzzle starting (and ending) at a given 
index

Inputs:  - an index from which to start the tree search

returns true if we found any new solutions

**/
void Solver::generateSolutions(int x, int y)
{
	continueWordFrom(x,y);
}

//sums up the elements in the input vec and returns it
template<class T>
T vecSum(vector<T> & vec)
{
	T sum = 0;
	for (T & n : vec)
	{
		sum += n;
	}
	return sum;
}

/*
google the words individually, to get their average results.

google all of the words together (with quotes), and record the number of 
relative results as a meter of fitness

*/