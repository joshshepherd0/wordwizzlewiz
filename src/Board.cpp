#include "../include/Board.hpp"

Board::Board()
{

}

Board::Board(vector<vector<char>> board)
{
	// deep copy the parameter board
	boardMask_ = board_ = board;
	height = board_.size();
	if (!height)
	{
		width = 0;
	}
	else
	{
		width = board_[0].size();
	}
}

Board::Board(int newWidth, int newHeight)
{
	width = newWidth;
	height = newHeight;
	board_.resize(newHeight);
	if (newHeight > 0)
	{
		board_[0].resize(newWidth);
	}
	boardMask_ = board_;
}

void Board::setBoard(vector<vector<char>> board)
{
	boardMask_ = board_ = board;
	height = board_.size();

	// ensure we don't attempt to access a row that doesn't exist
	//width = height ? board[0].size() : 0;
	if (!height)
	{
		width = 0;
	}
	else
	{
		width = board_[0].size();
	}
}

/*
retrieves the letter corresponding to the location of a traditional scalar index

precondition: (x,y) is on the board (i.e. isOnBoard(x,y))
*/
char Board::getLetter(int index)
{
	int x = index / width;
	int y = index % width;
	return boardMask_[x][y];
}

/*
precondition: (x,y) is on the board (i.e. isOnBoard(x,y))
*/
char Board::getLetter(int x, int y)
{
	return boardMask_[x][y];
}

/*
Sets the board at the given location equal to space (' ')

precondition: (x,y) is on the board (i.e. isOnBoard(x,y))
*/
void Board::clear(int x, int y)
{
	boardMask_[x][y] = ' ';
}

/*
Restore the board at the given location to its original lettering (or lack thereof)

precondition: (x,y) is on the board (i.e. isOnBoard(x,y))
*/
void Board::restore(int x, int y)
{
	boardMask_[x][y] = board_[x][y];
}

/*
precondition: (x,y) is on the board (i.e. isOnBoard(x,y))
*/
void Board::setLetter(int x, int y, char letter)
{
	board_[x][y] = letter;
}

/*
print out the board mask's contents
*/
void Board::print()
{
	for (vector<char> row : boardMask_)
	{
		for (char xCh : row)
		{
			cout << ' ' << xCh;
		}
		cout << endl;
	}
}

/*
returns true if the board mask consists only of spaces (ie. ' ')
*/
bool Board::isEmpty()
{
	for (vector<char> row : boardMask_)
	{
		for (char xCh : row)
		{
			if (xCh != ' ')
			{
				return false;
			}
		}
	}
	return true;
}

/*
returns true if the provided (x,y) coordinates can be found on the board
*/
bool Board::isOnBoard(int x, int y)
{
	if (x < 0 || x >= width ||
		y < 0 || y >= height)
	{
		// (x,y) is not on the board
		return false;
	}
	return true;
}

/*
sets the board mask back to the original unadulterated board
*/
void Board::resetMask()
{
	boardMask_ = board_;
}

vector<vector<char>> toLowerCase(vector<vector<char>> vec)
{
	// TODO
	return vec;
}
