# Word Wizzle Wiz #



### What is this repository for? ###

* Word Wizzle Wiz is a program written to algorithmically produce all of the solutions to any of the puzzles in the popular phone game, Word Whizzle [(itunes)](https://itunes.apple.com/us/app/wordwhizzle/id1078643602?mt=8)

### How do I get set up? ###

* Download/clone the repository
* navigate to the root directory
* run "make run" or "make test1" (or test2, ... test5)