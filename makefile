## 
## Joshua Shepherd
## 2017-02-17
##

CC=g++
#CFLAGS=
CPPFLAGS=-std=c++11 -O3 -Wall
LDFLAGS=-O3 -Wall
INCLUDE=Board.hpp Dict.hpp Solver.hpp WizzleWiz.hpp
SRC=main.cpp Solver.cpp Dict.cpp Board.cpp WizzleWiz.cpp
OBJ=$(SRC:.cpp=.o)
TARGET=whizzlewiz
bin_dir = bin
include_dir = include
src_dir = src

INCLUDES = $(addprefix $(include_dir)/, $(INCLUDE))
SRCS = $(addprefix $(src_dir)/, $(SRC))
OBJS = $(addprefix $(bin_dir)/, $(OBJ))

all: $(TARGET)
	
$(TARGET): $(OBJS)
	$(CC) -o $(TARGET) $(OBJS) $(LDFLAGS)

$(OBJS): $(bin_dir)/%.o : $(src_dir)/%.cpp $(INCLUDES)
	$(CC) -c $(CPPFLAGS) $< -o $@

run: all
	./$(TARGET)

testall: test1 test2 test3 test4
	
test1: all
	./$(TARGET) 1
	
test2: all
	./$(TARGET) 2
	
test3: all
	./$(TARGET) 3
	
test4: all
	./$(TARGET) 4
	
test5: all
	./$(TARGET) 5
		
clean:
	rm -rf $(OBJS) $(TARGET)

.PHONY: clean \
	all \
	run \
	test*