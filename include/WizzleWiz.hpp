#ifndef WIZZLE_WIZ_HPP
#define WIZZLE_WIZ_HPP

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fstream>
#include "Solver.hpp"

class WizzleWiz
{
public:
	// run the solver with the board and word lengths corresponding to 'index'
	int runTest(int index);

private:
	const string fname = "dictionary.txt";
};

#endif