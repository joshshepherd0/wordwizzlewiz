#ifndef DICT_HPP
#define DICT_HPP

#include <iostream>
#include <map>
#include <string>
#include <fstream>
#include <algorithm>

// TODO reconsider using namespace std
using namespace std;

class Dict
{
public:
	//clears all of the words from dict_, setting dict_.size() equal to 0
	void clear();
	void addWord(string word);
	int size();

	//returns true if a particular prefix exists in the dictionary map
	bool containsPrefix(string prefix);
	//returns true if an entire word exists in the dictionary map
	bool containsWord(string word);

	// load words from fname (newline delimited) into dict_
	int loadDictionary(string fname);
private:
	//uses a map in order to have easy prefix searches (eg. it's simple (and 
	//	relatively fast) to determine if is there a word in this dictionary that
	//	starts with "cedj")
	map<string, bool> dict_;
};


#endif
