#ifndef BOARD_HPP
#define BOARD_HPP

#include <thread>
#include <vector>
#include <iostream>

// TODO reconsider using namespace std
using namespace std;
/*

This board class represents the virtual board from Word Whizzle. As the Solver 
class clears segments of the board, the 'boardMask_' changes (into char==' ') 
but the 'board_' remains unchanged so it can be easily restore()'d

*/
class Board
{
public:
	Board();
	Board(vector<vector<char>> board);
	Board(int width, int height);

	void setBoard(vector<vector<char>> board);
	char getLetter(int index);
	char getLetter(int x, int y);
	void clear(int x, int y);
	void restore(int x, int y);
	void setLetter(int x, int y, char letter);
	int width = 0, height = 0;

	void print();
	bool isEmpty();
	bool isOnBoard(int x, int y);
	void resetMask();

private:
	vector<vector<char>> board_, boardMask_;

};

#endif