#ifndef SOLVER_HPP
#define SOLVER_HPP

#include <algorithm>
#include <string>
#include <fstream>
#include "Board.hpp"
#include "Dict.hpp"

/*
Given
	- a puzzle board (board_)
	- a dictionary file (fname -> dict_)
	- the lengths of the words (targetWordLengths_)
this class facilitates the 

The basic idea is to fill a string with the letters of each of its neighbors 
recursively until we 
	- run out of nearby letters
	- or get a string that's too long.
So we start from each square on the board and try to use the letters that 
branch out from our position to make every word possible.
*/

class Solver
{
public:
	Solver(Board board, vector<int> targetWordLengths);

	void printUniqSolutions();

	// set the lengths of the words that we are trying to guess
	void setTargetWordLengths(vector<int> & targWordLengths);
	void setBoard(Board board);

	int loadDictionary(string fname);
	void generateSolutions();
	void generateSolutions(int x, int y);
	

private:
	//keeps track of all possible combinations of words that solve this puzzle
	vector<vector<string>> uniqSolutions_;

	//keeps track of all duplicate solutions (for print-out info in results)
	vector<int> numSolutions_;

	// the set of words that may potentially make a complete solution
	vector<string> currentSolution_;

	//custom class to access the dictionary data 
	Dict dict_; //std::map (lower_bound())

	// declares the board that we will run our searches through
	Board board_;

	// the beginning of the current potential word
	string prefix_ = "";

	//the maximum value in the dynamic targetWordLengths_
	int maxAvailableLength_;

	//stores the length of each word we're currently looking for
	vector<int> targetWordLengths_;

	//////////////////////////////////////////////////////////////
	//functions
	
	// try to find longer words and more words in these locations
	void continueWordFrom(int x, int y);
	void continueWordFromAllPos();
	void continueWordInAllDirs(int x, int y);

	// returns true if we can conceivably fit 'len' into the remaining letters on
	// the board
	bool isAvailableLength(int len);
	// are there any letters left on the board? if there are, what is the longest 
	// word left? 
	void updateMaxAvailableLength();

	// continue manipulating the board, just now, with a fresh word (len=0)
	void findNextWord();
	// save the completed solution to uniqSolutions (if it's not already there)
	void saveSolution();

	//returns true if the provided solution is not already present in "uniqSolutions_"
	bool solutionIsUniq(vector<string> & solution);

	//if the parameter solution is unique, return -1
	// otherwise, return the index of "uniqSolutions_" that this solution duplicates
	int getSolutionIndex(vector<string> & solution);

};

//sums up the elements in the input vec and returns it
template<class T>
T vecSum(vector<T> & vec);

#endif